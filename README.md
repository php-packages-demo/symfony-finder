# symfony-finder

The Finder component finds files and directories via an intuitive fluent interface. https://symfony.com/doc/current/components/finder.html

![Debian popularity](https://qa.debian.org/cgi-bin/popcon-png?packages=php-symfony-filesystem+php-symfony-console+php-symfony-process+php-symfony-finder+php-symfony-expression-language+php-symfony-cache+php-symfony-config+php-symfony-dependency-injection+php-symfony-event-dispatcher&show_installed=1&show_vote=0&show_old=0&show_recent=0&show_nofiles=0&want_percent=0&want_legend=1&want_ticks=1&from_date=&to_date=&hlght_date=&date_fmt=%25Y)

# Unofficial documentation
* [*Hidden Gems of PHP Packages: Symfony\Finder and SplFileInfo*
  ](https://www.tomasvotruba.cz/blog/2018/08/13/hidden-gems-of-php-packages-symfony-finder-and-spl-file-info/)
  2018-08 Tomas Votruba
